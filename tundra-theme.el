;;; tundra-theme.el --- An expansion of the excellent Tundra theme

;; Copyright (C) 2022-present Ashton Wiersdorf <ashton.wiersdorf@pobox.com> (https://lambdaland.org)

;; Title: Tundra Theme
;; Project: tundra
;; Version: 0.0.1
;; URL: https://sr.ht/~ashton314/tundra-theme/
;; Author: Ashton Wiersdorf <ashton.wiersdorf@pobox.com>
;; Package-Requires: ((emacs "24"))
;; License: MIT

;;; Commentary:

;; An expansion of the excellent Tundra theme for Emacs

;;; References:
;; Tundra
;;   https://www.tundratheme.com/

;;; Code:

(unless (>= emacs-major-version 24)
  (error "Tundraic forest theme requires Emacs 24 or later!"))

(deftheme tundraic-forest "A gentle, greener version of tundra theme")

;; FIXME: CUSTOMIZATION ENDS HERE

(defgroup tundra nil
  "Tundra theme customizations.
  The theme has to be reloaded after changing anything in this group."
  :group 'faces)

(defcustom tundra-comment-brightness 10
  "Allows to define a custom comment color brightness with percentage adjustments from 0% - 20%.
  As of version 0.4.0, this variable is obsolete/deprecated and has no effect anymore and will be removed in version 1.0.0!
  The comment color brightness has been increased by 10% by default.
  Please see https://github.com/arcticicestudio/tundra-emacs/issues/73 for more details."
  :type 'integer
  :group 'tundra)

(make-obsolete-variable
  'tundra-comment-brightness
  "The custom color brightness feature has been deprecated and will be removed in version 1.0.0!
  The comment color brightness has been increased by 10% by default.
  Please see https://github.com/arcticicestudio/tundra-emacs/issues/73 for more details."
  "0.4.0")

(defcustom tundra-region-highlight nil
  "Allows to set a region highlight style based on the Tundra components.
  Valid styles are
    - 'snowstorm' - Uses 'tundra0' as foreground- and 'tundra4' as background color
    - 'frost' - Uses 'tundra0' as foreground- and 'tundra8' as background color"
  :type 'string
  :group 'tundra)

(defcustom tundra-uniform-mode-lines nil
  "Enables uniform activate- and inactive mode lines using 'tundra3' as background."
  :type 'boolean
  :group 'tundra)

(setq tundra-theme--brightened-comments '("#4c566a" "#4e586d" "#505b70" "#525d73" "#556076" "#576279" "#59647c" "#5b677f" "#5d6982" "#5f6c85" "#616e88" "#63718b" "#66738e" "#687591" "#6a7894" "#6d7a96" "#6f7d98" "#72809a" "#75829c" "#78859e" "#7b88a1"))

(defun tundra-theme--brightened-comment-color (percent)
  "Returns the brightened comment color for the given percent.
  The value must be greater or equal to 0 and less or equal to 20, otherwise the default 'tundra3' color is used.
  As of version 0.4.0, this function is obsolete/deprecated and has no effect anymore and will be removed in version 1.0.0!
  The comment color brightness has been increased by 10% by default.
  Please see https://github.com/arcticicestudio/tundra-emacs/issues/73 for more details."
  (nth 10 tundra-theme--brightened-comments))

(make-obsolete
  'tundra-theme--brightened-comment-color
  "The custom color brightness feature has been deprecated and will be removed in version 1.0.0!\
  The comment color brightness has been increased by 10% by default.\
  Please see https://github.com/arcticicestudio/tundra-emacs/issues/73 for more details."
  "0.4.0")

(defun tundra-display-truecolor-or-graphic-p ()
  "Returns whether the display can display tundra colors"
  (or (= (display-color-cells) 16777216) (display-graphic-p)))

;;;; Color Constants
(let ((class '((class color) (min-colors 89)))
  (tundra0 (if (tundra-display-truecolor-or-graphic-p) "#2E3440" nil))
  (tundra1 (if (tundra-display-truecolor-or-graphic-p) "#3B4252" "black"))
  (tundra2 (if (tundra-display-truecolor-or-graphic-p) "#434C5E" "#434C5E"))
  (tundra3 (if (tundra-display-truecolor-or-graphic-p) "#4C566A" "brightblack"))
  (tundra4 (if (tundra-display-truecolor-or-graphic-p) "#D8DEE9" "#D8DEE9"))
  (tundra5 (if (tundra-display-truecolor-or-graphic-p) "#E5E9F0" "white"))
  (tundra6 (if (tundra-display-truecolor-or-graphic-p) "#ECEFF4" "brightwhite"))
  (tundra7 (if (tundra-display-truecolor-or-graphic-p) "#8FBCBB" "cyan"))
  (tundra8 (if (tundra-display-truecolor-or-graphic-p) "#88C0D0" "brightcyan"))
  (tundra9 (if (tundra-display-truecolor-or-graphic-p) "#81A1C1" "blue"))
  (tundra10 (if (tundra-display-truecolor-or-graphic-p) "#5E81AC" "brightblue"))
  (tundra11 (if (tundra-display-truecolor-or-graphic-p) "#BF616A" "red"))
  (tundra12 (if (tundra-display-truecolor-or-graphic-p) "#D08770" "brightyellow"))
  (tundra13 (if (tundra-display-truecolor-or-graphic-p) "#EBCB8B" "yellow"))
  (tundra14 (if (tundra-display-truecolor-or-graphic-p) "#A3BE8C" "green"))
  (tundra15 (if (tundra-display-truecolor-or-graphic-p) "#B48EAD" "magenta"))
  (tundra-annotation (if (tundra-display-truecolor-or-graphic-p) "#D08770" "brightyellow"))
  (tundra-attribute (if (tundra-display-truecolor-or-graphic-p) "#8FBCBB" "cyan"))
  (tundra-class (if (tundra-display-truecolor-or-graphic-p) "#8FBCBB" "cyan"))
  (tundra-comment (if (tundra-display-truecolor-or-graphic-p) (tundra-theme--brightened-comment-color tundra-comment-brightness) "brightblack"))
  (tundra-escape (if (tundra-display-truecolor-or-graphic-p) "#D08770" "brightyellow"))
  (tundra-method (if (tundra-display-truecolor-or-graphic-p) "#88C0D0" "brightcyan"))
  (tundra-keyword (if (tundra-display-truecolor-or-graphic-p) "#81A1C1" "blue"))
  (tundra-numeric (if (tundra-display-truecolor-or-graphic-p) "#B48EAD" "magenta"))
  (tundra-operator (if (tundra-display-truecolor-or-graphic-p) "#81A1C1" "blue"))
  (tundra-preprocessor (if (tundra-display-truecolor-or-graphic-p) "#5E81AC" "brightblue"))
  (tundra-punctuation (if (tundra-display-truecolor-or-graphic-p) "#D8DEE9" "#D8DEE9"))
  (tundra-regexp (if (tundra-display-truecolor-or-graphic-p) "#EBCB8B" "yellow"))
  (tundra-string (if (tundra-display-truecolor-or-graphic-p) "#A3BE8C" "green"))
  (tundra-tag (if (tundra-display-truecolor-or-graphic-p) "#81A1C1" "blue"))
  (tundra-variable (if (tundra-display-truecolor-or-graphic-p) "#D8DEE9" "#D8DEE9"))
  (tundra-region-highlight-foreground (if (or
    (string= tundra-region-highlight "frost")
    (string= tundra-region-highlight "snowstorm")) "#2E3440" nil))
  (tundra-region-highlight-background (if
    (string= tundra-region-highlight "frost") "#88C0D0"
      (if (string= tundra-region-highlight "snowstorm") "#D8DEE9" "#434C5E")))
  (tundra-uniform-mode-lines-background (if tundra-uniform-mode-lines "#4C566A" "#3B4252")))

;;;; +------------+
;;;; + Core Faces +
;;;; +------------+
  (custom-theme-set-faces
    'tundra
    ;; +--- Base ---+
    `(bold ((,class (:weight bold))))
    `(bold-italic ((,class (:weight bold :slant italic))))
    `(default ((,class (:foreground ,tundra4 :background ,tundra0))))
    `(error ((,class (:foreground ,tundra11 :weight bold))))
    `(escape-glyph ((,class (:foreground ,tundra12))))
    `(font-lock-builtin-face ((,class (:foreground ,tundra9))))
    `(font-lock-comment-face ((,class (:foreground ,tundra-comment))))
    `(font-lock-comment-delimiter-face ((,class (:foreground ,tundra-comment))))
    `(font-lock-constant-face ((,class (:foreground ,tundra9))))
    `(font-lock-doc-face ((,class (:foreground ,tundra-comment))))
    `(font-lock-function-name-face ((,class (:foreground ,tundra8))))
    `(font-lock-keyword-face ((,class (:foreground ,tundra9))))
    `(font-lock-negation-char-face ((,class (:foreground ,tundra9))))
    `(font-lock-preprocessor-face ((,class (:foreground ,tundra10 :weight bold))))
    `(font-lock-reference-face ((,class (:foreground ,tundra9))))
    `(font-lock-regexp-grouping-backslash ((,class (:foreground ,tundra13))))
    `(font-lock-regexp-grouping-construct ((,class (:foreground ,tundra13))))
    `(font-lock-string-face ((,class (:foreground ,tundra14))))
    `(font-lock-type-face ((,class (:foreground ,tundra7))))
    `(font-lock-variable-name-face ((,class (:foreground ,tundra4))))
    `(font-lock-warning-face ((,class (:foreground ,tundra13))))
    `(italic ((,class (:slant italic))))
    `(shadow ((,class (:foreground ,tundra3))))
    `(underline ((,class (:underline t))))
    `(warning ((,class (:foreground ,tundra13 :weight bold))))

    ;; +--- Syntax ---+
    ;; > C
    `(c-annotation-face ((,class (:foreground ,tundra-annotation))))

    ;; > diff
    `(diff-added ((,class (:foreground ,tundra14))))
    `(diff-changed ((,class (:foreground ,tundra13))))
    `(diff-context ((,class (:inherit default))))
    `(diff-file-header ((,class (:foreground ,tundra8))))
    `(diff-function ((,class (:foreground ,tundra7))))
    `(diff-header ((,class (:foreground ,tundra9 :weight bold))))
    `(diff-hunk-header ((,class (:foreground ,tundra9 :background ,tundra0))))
    `(diff-indicator-added ((,class (:foreground ,tundra14))))
    `(diff-indicator-changed ((,class (:foreground ,tundra13))))
    `(diff-indicator-removed ((,class (:foreground ,tundra11))))
    `(diff-nonexistent ((,class (:foreground ,tundra11))))
    `(diff-refine-added ((,class (:foreground ,tundra14))))
    `(diff-refine-changed ((,class (:foreground ,tundra13))))
    `(diff-refine-removed ((,class (:foreground ,tundra11))))
    `(diff-removed ((,class (:foreground ,tundra11))))

    ;; +--- UI ---+
    `(border ((,class (:foreground ,tundra4))))
    `(buffer-menu-buffer ((,class (:foreground ,tundra4 :weight bold))))
    `(button ((,class (:background ,tundra0 :foreground ,tundra8 :box (:line-width 2 :color ,tundra4 :style sunken-button)))))
    `(completions-annotations ((,class (:foreground ,tundra9))))
    `(completions-common-part ((,class (:foreground ,tundra8 :weight bold))))
    `(completions-first-difference ((,class (:foreground ,tundra11))))
    `(custom-button ((,class (:background ,tundra0 :foreground ,tundra8 :box (:line-width 2 :color ,tundra4 :style sunken-button)))))
    `(custom-button-mouse ((,class (:background ,tundra4 :foreground ,tundra0 :box (:line-width 2 :color ,tundra4 :style sunken-button)))))
    `(custom-button-pressed ((,class (:background ,tundra6 :foreground ,tundra0 :box (:line-width 2 :color ,tundra4 :style sunken-button)))))
    `(custom-button-pressed-unraised ((,class (:background ,tundra4 :foreground ,tundra0 :box (:line-width 2 :color ,tundra4 :style sunken-button)))))
    `(custom-button-unraised ((,class (:background ,tundra0 :foreground ,tundra8 :box (:line-width 2 :color ,tundra4 :style sunken-button)))))
    `(custom-changed ((,class (:foreground ,tundra13))))
    `(custom-comment ((,class (:foreground ,tundra-comment))))
    `(custom-comment-tag ((,class (:foreground ,tundra7))))
    `(custom-documentation ((,class (:foreground ,tundra4))))
    `(custom-group-tag ((,class (:foreground ,tundra8 :weight bold))))
    `(custom-group-tag-1 ((,class (:foreground ,tundra8 :weight bold))))
    `(custom-invalid ((,class (:foreground ,tundra11))))
    `(custom-modified ((,class (:foreground ,tundra13))))
    `(custom-rogue ((,class (:foreground ,tundra12 :background ,tundra2))))
    `(custom-saved ((,class (:foreground ,tundra14))))
    `(custom-set ((,class (:foreground ,tundra8))))
    `(custom-state ((,class (:foreground ,tundra14))))
    `(custom-themed ((,class (:foreground ,tundra8 :background ,tundra2))))
    `(cursor ((,class (:background ,tundra4))))
    `(fringe ((,class (:foreground ,tundra4 :background ,tundra0))))
    `(file-name-shadow ((,class (:inherit shadow))))
    `(header-line ((,class (:foreground ,tundra4 :background ,tundra2))))
    `(help-argument-name ((,class (:foreground ,tundra8))))
    `(highlight ((,class (:foreground ,tundra8 :background ,tundra2))))
    `(hl-line ((,class (:background ,tundra1))))
    `(info-menu-star ((,class (:foreground ,tundra9))))
    `(isearch ((,class (:foreground ,tundra0 :background ,tundra8))))
    `(isearch-fail ((,class (:foreground ,tundra11))))
    `(link ((,class (:underline t))))
    `(link-visited ((,class (:underline t))))
    `(linum ((,class (:foreground ,tundra3 :background ,tundra0))))
    `(linum-relative-current-face ((,class (:foreground ,tundra3 :background ,tundra0))))
    `(match ((,class (:inherit isearch))))
    `(message-cited-text ((,class (:foreground ,tundra4))))
    `(message-header-cc ((,class (:foreground ,tundra9))))
    `(message-header-name ((,class (:foreground ,tundra7))))
    `(message-header-newsgroup ((,class (:foreground ,tundra14))))
    `(message-header-other ((,class (:foreground ,tundra4))))
    `(message-header-subject ((,class (:foreground ,tundra8))))
    `(message-header-to ((,class (:foreground ,tundra9))))
    `(message-header-xheader ((,class (:foreground ,tundra13))))
    `(message-mml ((,class (:foreground ,tundra10))))
    `(message-separator ((,class (:inherit shadow))))
    `(minibuffer-prompt ((,class (:foreground ,tundra8 :weight bold))))
    `(mm-command-output ((,class (:foreground ,tundra8))))
    `(mode-line ((,class (:foreground ,tundra8 :background ,tundra3))))
    `(mode-line-buffer-id ((,class (:weight bold))))
    `(mode-line-highlight ((,class (:inherit highlight))))
    `(mode-line-inactive ((,class (:foreground ,tundra4 :background ,tundra-uniform-mode-lines-background))))
    `(next-error ((,class (:inherit error))))
    `(nobreak-space ((,class (:foreground ,tundra3))))
    `(outline-1 ((,class (:foreground ,tundra8 :weight bold))))
    `(outline-2 ((,class (:inherit outline-1))))
    `(outline-3 ((,class (:inherit outline-1))))
    `(outline-4 ((,class (:inherit outline-1))))
    `(outline-5 ((,class (:inherit outline-1))))
    `(outline-6 ((,class (:inherit outline-1))))
    `(outline-7 ((,class (:inherit outline-1))))
    `(outline-8 ((,class (:inherit outline-1))))
    `(package-description ((,class (:foreground ,tundra4))))
    `(package-help-section-name ((,class (:foreground ,tundra8 :weight bold))))
    `(package-name ((,class (:foreground ,tundra8))))
    `(package-status-available ((,class (:foreground ,tundra7))))
    `(package-status-avail-obso ((,class (:foreground ,tundra7 :slant italic))))
    `(package-status-built-in ((,class (:foreground ,tundra9))))
    `(package-status-dependency ((,class (:foreground ,tundra8 :slant italic))))
    `(package-status-disabled ((,class (:foreground ,tundra3))))
    `(package-status-external ((,class (:foreground ,tundra12 :slant italic))))
    `(package-status-held ((,class (:foreground ,tundra4 :weight bold))))
    `(package-status-new ((,class (:foreground ,tundra14))))
    `(package-status-incompat ((,class (:foreground ,tundra11))))
    `(package-status-installed ((,class (:foreground ,tundra7 :weight bold))))
    `(package-status-unsigned ((,class (:underline ,tundra13))))
    `(query-replace ((,class (:foreground ,tundra8 :background ,tundra2))))
    `(region ((,class (:foreground ,tundra-region-highlight-foreground :background ,tundra-region-highlight-background))))
    `(scroll-bar ((,class (:background ,tundra3))))
    `(secondary-selection ((,class (:background ,tundra2))))

    ;; `show-paren-match-face` and `show-paren-mismatch-face` are deprecated since Emacs version 22.1 and were
    ;; removed in Emacs 25.
    ;; https://github.com/arcticicestudio/tundra-emacs/issues/75
    ;; http://git.savannah.gnu.org/cgit/emacs.git/commit/?id=c430f7e23fc2c22f251ace4254e37dea1452dfc3
    ;; https://github.com/emacs-mirror/emacs/commit/c430f7e23fc2c22f251ace4254e37dea1452dfc3
    `(show-paren-match-face ((,class (:foreground ,tundra0 :background ,tundra8))))
    `(show-paren-mismatch-face ((,class (:background ,tundra11))))

    `(show-paren-match ((,class (:foreground ,tundra0 :background ,tundra8))))
    `(show-paren-mismatch ((,class (:background ,tundra11))))
    `(success ((,class (:foreground ,tundra14))))
    `(term ((,class (:foreground ,tundra4 :background ,tundra0))))
    `(term-color-black ((,class (:foreground ,tundra1 :background ,tundra1))))
    `(term-color-white ((,class (:foreground ,tundra5 :background ,tundra5))))
    `(term-color-cyan ((,class (:foreground ,tundra7 :background ,tundra7))))
    `(term-color-blue ((,class (:foreground ,tundra8 :background ,tundra8))))
    `(term-color-red ((,class (:foreground ,tundra11 :background ,tundra11))))
    `(term-color-yellow ((,class (:foreground ,tundra13 :background ,tundra13))))
    `(term-color-green ((,class (:foreground ,tundra14 :background ,tundra14))))
    `(term-color-magenta ((,class (:foreground ,tundra15 :background ,tundra15))))
    `(tool-bar ((,class (:foreground ,tundra4 :background ,tundra3))))
    `(tooltip ((,class (:foreground ,tundra0 :background ,tundra4))))
    `(trailing-whitespace ((,class (:foreground ,tundra3))))
    `(tty-menu-disabled-face ((,class (:foreground ,tundra1))))
    `(tty-menu-enabled-face ((,class (:background ,tundra2 foreground ,tundra4))))
    `(tty-menu-selected-face ((,class (:foreground ,tundra8 :underline t))))
    `(undo-tree-visualizer-current-face ((,class (:foreground ,tundra8))))
    `(undo-tree-visualizer-default-face ((,class (:foreground ,tundra4))))
    `(undo-tree-visualizer-unmodified-face ((,class (:foreground ,tundra4))))
    `(undo-tree-visualizer-register-face ((,class (:foreground ,tundra9))))
    `(vc-conflict-state ((,class (:foreground ,tundra12))))
    `(vc-edited-state ((,class (:foreground ,tundra13))))
    `(vc-locally-added-state ((,class (:underline ,tundra14))))
    `(vc-locked-state ((,class (:foreground ,tundra10))))
    `(vc-missing-state ((,class (:foreground ,tundra11))))
    `(vc-needs-update-state ((,class (:foreground ,tundra12))))
    `(vc-removed-state ((,class (:foreground ,tundra11))))
    `(vc-state-base ((,class (:foreground ,tundra4))))
    `(vc-up-to-date-state ((,class (:foreground ,tundra8))))
    `(vertical-border ((,class (:foreground ,tundra2))))
    `(which-func ((,class (:foreground ,tundra8))))
    `(whitespace-big-indent ((,class (:foreground ,tundra3 :background ,tundra0))))
    `(whitespace-empty ((,class (:foreground ,tundra3 :background ,tundra0))))
    `(whitespace-hspace ((,class (:foreground ,tundra3 :background ,tundra0))))
    `(whitespace-indentation ((,class (:foreground ,tundra3 :background ,tundra0))))
    `(whitespace-line ((,class (:background ,tundra0))))
    `(whitespace-newline ((,class (:foreground ,tundra3 :background ,tundra0))))
    `(whitespace-space ((,class (:foreground ,tundra3 :background ,tundra0))))
    `(whitespace-space-after-tab ((,class (:foreground ,tundra3 :background ,tundra0))))
    `(whitespace-space-before-tab ((,class (:foreground ,tundra3 :background ,tundra0))))
    `(whitespace-tab ((,class (:foreground ,tundra3 :background ,tundra0))))
    `(whitespace-trailing ((,class (:inherit trailing-whitespace))))
    `(widget-button-pressed ((,class (:foreground ,tundra9 :background ,tundra1))))
    `(widget-documentation ((,class (:foreground ,tundra4))))
    `(widget-field ((,class (:background ,tundra2 :foreground ,tundra4))))
    `(widget-single-line-field ((,class (:background ,tundra2 :foreground ,tundra4))))
    `(window-divider ((,class (:background ,tundra3))))
    `(window-divider-first-pixel ((,class (:background ,tundra3))))
    `(window-divider-last-pixel ((,class (:background ,tundra3))))

    ;;;; +-----------------+
    ;;;; + Package Support +
    ;;;; +-----------------+
    ;; +--- Syntax ---+
    ;; > Auctex
    `(font-latex-bold-face ((,class (:inherit bold))))
    `(font-latex-italic-face ((,class (:inherit italic))))
    `(font-latex-math-face ((,class (:foreground ,tundra8))))
    `(font-latex-sectioning-0-face ((,class (:foreground ,tundra8 :weight bold))))
    `(font-latex-sectioning-1-face ((,class (:inherit font-latex-sectioning-0-face))))
    `(font-latex-sectioning-2-face ((,class (:inherit font-latex-sectioning-0-face))))
    `(font-latex-sectioning-3-face ((,class (:inherit font-latex-sectioning-0-face))))
    `(font-latex-sectioning-4-face ((,class (:inherit font-latex-sectioning-0-face))))
    `(font-latex-sectioning-5-face ((,class (:inherit font-latex-sectioning-0-face))))
    `(font-latex-script-char-face ((,class (:inherit font-lock-warning-face))))
    `(font-latex-string-face ((,class (:inherit font-lock-string-face))))
    `(font-latex-warning-face ((,class (:inherit font-lock-warning-face))))

    ;; > Elixir
    `(elixir-attribute-face ((,class (:foreground ,tundra-annotation))))
    `(elixir-atom-face ((,class (:foreground ,tundra4 :weight bold))))

    ;; > Enhanced Ruby
    `(enh-ruby-heredoc-delimiter-face ((,class (:foreground ,tundra14))))
    `(enh-ruby-op-face ((,class (:foreground ,tundra9))))
    `(enh-ruby-regexp-delimiter-face ((,class (:foreground ,tundra13))))
    `(enh-ruby-regexp-face ((,class (:foreground ,tundra13))))
    `(enh-ruby-string-delimiter-face ((,class (:foreground ,tundra14))))
    `(erm-syn-errline ((,class (:foreground ,tundra11 :underline t))))
    `(erm-syn-warnline ((,class (:foreground ,tundra13 :underline t))))

    ;; > Java Development Environment for Emacs
    `(jdee-db-active-breakpoint-face ((,class (:background ,tundra2 :weight bold))))
    `(jdee-bug-breakpoint-cursor ((,class (:background ,tundra2))))
    `(jdee-db-requested-breakpoint-face ((,class (:foreground ,tundra13 :background ,tundra2 :weight bold))))
    `(jdee-db-spec-breakpoint-face ((,class (:foreground ,tundra14 :background ,tundra2 :weight bold))))
    `(jdee-font-lock-api-face ((,class (:foreground ,tundra4))))
    `(jdee-font-lock-code-face ((,class (:slant italic))))
    `(jdee-font-lock-constant-face ((,class (:foreground ,tundra-keyword))))
    `(jdee-font-lock-constructor-face ((,class (:foreground ,tundra-method))))
    `(jdee-font-lock-doc-tag-face ((,class (:foreground ,tundra7))))
    `(jdee-font-lock-link-face ((,class (:underline t))))
    `(jdee-font-lock-modifier-face ((,class (:foreground ,tundra-keyword))))
    `(jdee-font-lock-number-face ((,class (:foreground ,tundra-numeric))))
    `(jdee-font-lock-operator-fac ((,class (:foreground ,tundra-operator))))
    `(jdee-font-lock-package-face ((,class (:foreground ,tundra-class))))
    `(jdee-font-lock-pre-face ((,class (:foreground ,tundra-comment :slant italic))))
    `(jdee-font-lock-private-face ((,class (:foreground ,tundra-keyword))))
    `(jdee-font-lock-public-face ((,class (:foreground ,tundra-keyword))))
    `(jdee-font-lock-variable-face ((,class (:foreground ,tundra-variable))))

    ;; > JavaScript 2
    `(js2-function-call ((,class (:foreground ,tundra8))))
    `(js2-private-function-call ((,class (:foreground ,tundra8))))
    `(js2-jsdoc-html-tag-delimiter ((,class (:foreground ,tundra6))))
    `(js2-jsdoc-html-tag-name ((,class (:foreground ,tundra9))))
    `(js2-external-variable ((,class (:foreground ,tundra4))))
    `(js2-function-param ((,class (:foreground ,tundra4))))
    `(js2-jsdoc-value ((,class (:foreground ,tundra-comment))))
    `(js2-jsdoc-tag ((,class (:foreground ,tundra7))))
    `(js2-jsdoc-type ((,class (:foreground ,tundra7))))
    `(js2-private-member ((,class (:foreground ,tundra4))))
    `(js2-object-property ((,class (:foreground ,tundra4))))
    `(js2-error ((,class (:foreground ,tundra11))))
    `(js2-warning ((,class (:foreground ,tundra13))))
    `(js2-instance-member ((,class (:foreground ,tundra4))))

    ;; > JavaScript 3
    `(js3-error-face ((,class (:foreground ,tundra11))))
    `(js3-external-variable-face ((,class (:foreground ,tundra4))))
    `(js3-function-param-face ((,class (:foreground ,tundra4))))
    `(js3-instance-member-face ((,class (:foreground ,tundra4))))
    `(js3-jsdoc-html-tag-delimiter-face ((,class (:foreground ,tundra6))))
    `(js3-jsdoc-html-tag-name-face ((,class (:foreground ,tundra9))))
    `(js3-jsdoc-tag-face ((,class (:foreground ,tundra9))))
    `(js3-jsdoc-type-face ((,class (:foreground ,tundra7))))
    `(js3-jsdoc-value-face ((,class (:foreground ,tundra4))))
    `(js3-magic-paren-face ((,class (:inherit show-paren-match-face))))
    `(js3-private-function-call-face ((,class (:foreground ,tundra8))))
    `(js3-private-member-face ((,class (:foreground ,tundra4))))
    `(js3-warning-face ((,class (:foreground ,tundra13))))

    ;; > Markdown
    `(markdown-blockquote-face ((,class (:foreground ,tundra-comment))))
    `(markdown-bold-face ((,class (:inherit bold))))
    `(markdown-header-face-1 ((,class (:foreground ,tundra8))))
    `(markdown-header-face-2 ((,class (:foreground ,tundra8))))
    `(markdown-header-face-3 ((,class (:foreground ,tundra8))))
    `(markdown-header-face-4 ((,class (:foreground ,tundra8))))
    `(markdown-header-face-5 ((,class (:foreground ,tundra8))))
    `(markdown-header-face-6 ((,class (:foreground ,tundra8))))
    `(markdown-inline-code-face ((,class (:foreground ,tundra7))))
    `(markdown-italic-face ((,class (:inherit italic))))
    `(markdown-link-face ((,class (:foreground ,tundra8))))
    `(markdown-markup-face ((,class (:foreground ,tundra9))))
    `(markdown-reference-face ((,class (:inherit markdown-link-face))))
    `(markdown-url-face ((,class (:foreground ,tundra4 :underline t))))

    ;; > Rainbow Delimeters
    `(rainbow-delimiters-depth-1-face ((,class :foreground ,tundra7)))
    `(rainbow-delimiters-depth-2-face ((,class :foreground ,tundra8)))
    `(rainbow-delimiters-depth-3-face ((,class :foreground ,tundra9)))
    `(rainbow-delimiters-depth-4-face ((,class :foreground ,tundra10)))
    `(rainbow-delimiters-depth-5-face ((,class :foreground ,tundra12)))
    `(rainbow-delimiters-depth-6-face ((,class :foreground ,tundra13)))
    `(rainbow-delimiters-depth-7-face ((,class :foreground ,tundra14)))
    `(rainbow-delimiters-depth-8-face ((,class :foreground ,tundra15)))
    `(rainbow-delimiters-unmatched-face ((,class :foreground ,tundra11)))

    ;; > Web Mode
    `(web-mode-attr-tag-custom-face ((,class (:foreground ,tundra-attribute))))
    `(web-mode-builtin-face ((,class (:foreground ,tundra-keyword))))
    `(web-mode-comment-face ((,class (:foreground ,tundra-comment))))
    `(web-mode-comment-keyword-face ((,class (:foreground ,tundra-comment))))
    `(web-mode-constant-face ((,class (:foreground ,tundra-variable))))
    `(web-mode-css-at-rule-face ((,class (:foreground ,tundra-annotation))))
    `(web-mode-css-function-face ((,class (:foreground ,tundra-method))))
    `(web-mode-css-property-name-face ((,class (:foreground ,tundra-keyword))))
    `(web-mode-css-pseudo-class-face ((,class (:foreground ,tundra-class))))
    `(web-mode-css-selector-face ((,class (:foreground ,tundra-keyword))))
    `(web-mode-css-string-face ((,class (:foreground ,tundra-string))))
    `(web-mode-doctype-face ((,class (:foreground ,tundra-preprocessor))))
    `(web-mode-function-call-face ((,class (:foreground ,tundra-method))))
    `(web-mode-function-name-face ((,class (:foreground ,tundra-method))))
    `(web-mode-html-attr-name-face ((,class (:foreground ,tundra-attribute))))
    `(web-mode-html-attr-equal-face ((,class (:foreground ,tundra-punctuation))))
    `(web-mode-html-attr-value-face ((,class (:foreground ,tundra-string))))
    `(web-mode-html-entity-face ((,class (:foreground ,tundra-keyword))))
    `(web-mode-html-tag-bracket-face ((,class (:foreground ,tundra-punctuation))))
    `(web-mode-html-tag-custom-face ((,class (:foreground ,tundra-tag))))
    `(web-mode-html-tag-face ((,class (:foreground ,tundra-tag))))
    `(web-mode-html-tag-namespaced-face ((,class (:foreground ,tundra-keyword))))
    `(web-mode-json-key-face ((,class (:foreground ,tundra-class))))
    `(web-mode-json-string-face ((,class (:foreground ,tundra-string))))
    `(web-mode-keyword-face ((,class (:foreground ,tundra-keyword))))
    `(web-mode-preprocessor-face ((,class (:foreground ,tundra-preprocessor))))
    `(web-mode-string-face ((,class (:foreground ,tundra-string))))
    `(web-mode-symbol-face ((,class (:foreground ,tundra-variable))))
    `(web-mode-type-face ((,class (:foreground ,tundra-class))))
    `(web-mode-warning-face ((,class (:inherit ,font-lock-warning-face))))
    `(web-mode-variable-name-face ((,class (:foreground ,tundra-variable))))

    ;; +--- UI ---+
    ;; > Anzu
    `(anzu-mode-line ((,class (:foreground, tundra8))))
    `(anzu-mode-line-no-match ((,class (:foreground, tundra11))))

    ;; > Avy
    `(avy-lead-face ((,class (:background ,tundra11 :foreground ,tundra5))))
    `(avy-lead-face-0 ((,class (:background ,tundra10 :foreground ,tundra5))))
    `(avy-lead-face-1 ((,class (:background ,tundra3 :foreground ,tundra5))))
    `(avy-lead-face-2 ((,class (:background ,tundra15 :foreground ,tundra5))))

    ;; > Company
    `(company-echo-common ((,class (:foreground ,tundra0 :background ,tundra4))))
    `(company-preview ((,class (:foreground ,tundra4 :background ,tundra10))))
    `(company-preview-common ((,class (:foreground ,tundra0 :background ,tundra8))))
    `(company-preview-search ((,class (:foreground ,tundra0 :background ,tundra8))))
    `(company-scrollbar-bg ((,class (:foreground ,tundra1 :background ,tundra1))))
    `(company-scrollbar-fg ((,class (:foreground ,tundra2 :background ,tundra2))))
    `(company-template-field ((,class (:foreground ,tundra0 :background ,tundra7))))
    `(company-tooltip ((,class (:foreground ,tundra4 :background ,tundra2))))
    `(company-tooltip-annotation ((,class (:foreground ,tundra12))))
    `(company-tooltip-annotation-selection ((,class (:foreground ,tundra12 :weight bold))))
    `(company-tooltip-common ((,class (:foreground ,tundra8))))
    `(company-tooltip-common-selection ((,class (:foreground ,tundra8 :background ,tundra3))))
    `(company-tooltip-mouse ((,class (:inherit highlight))))
    `(company-tooltip-selection ((,class (:background ,tundra3 :weight bold))))

    ;; > diff-hl
   `(diff-hl-change ((,class (:background ,tundra13))))
   `(diff-hl-insert ((,class (:background ,tundra14))))
   `(diff-hl-delete ((,class (:background ,tundra11))))
   
    ;; > Evil
    `(evil-ex-info ((,class (:foreground ,tundra8))))
    `(evil-ex-substitute-replacement ((,class (:foreground ,tundra9))))
    `(evil-ex-substitute-matches ((,class (:inherit isearch))))

    ;; > Flycheck
    `(flycheck-error ((,class (:underline (:style wave :color ,tundra11)))))
    `(flycheck-fringe-error ((,class (:foreground ,tundra11 :weight bold))))
    `(flycheck-fringe-info ((,class (:foreground ,tundra8 :weight bold))))
    `(flycheck-fringe-warning ((,class (:foreground ,tundra13 :weight bold))))
    `(flycheck-info ((,class (:underline (:style wave :color ,tundra8)))))
    `(flycheck-warning ((,class (:underline (:style wave :color ,tundra13)))))

    ;; > Git Gutter
    `(git-gutter:modified ((,class (:foreground ,tundra13))))
    `(git-gutter:added ((,class (:foreground ,tundra14))))
    `(git-gutter:deleted ((,class (:foreground ,tundra11))))

    ;; > Git Gutter Plus
    `(git-gutter+-modified ((,class (:foreground ,tundra13))))
    `(git-gutter+-added ((,class (:foreground ,tundra14))))
    `(git-gutter+-deleted ((,class (:foreground ,tundra11))))

    ;; > Helm
    `(helm-bookmark-addressbook ((,class (:foreground ,tundra7))))
    `(helm-bookmark-directory ((,class (:foreground ,tundra9))))
    `(helm-bookmark-file ((,class (:foreground ,tundra8))))
    `(helm-bookmark-gnus ((,class (:foreground ,tundra10))))
    `(helm-bookmark-info ((,class (:foreground ,tundra14))))
    `(helm-bookmark-man ((,class (:foreground ,tundra4))))
    `(helm-bookmark-w3m ((,class (:foreground ,tundra9))))
    `(helm-buffer-directory ((,class (:foreground ,tundra9))))
    `(helm-buffer-file ((,class (:foreground ,tundra8))))
    `(helm-buffer-not-saved ((,class (:foreground ,tundra13))))
    `(helm-buffer-process ((,class (:foreground ,tundra10))))
    `(helm-candidate-number ((,class (:foreground ,tundra4 :weight bold))))
    `(helm-candidate-number-suspended ((,class (:foreground ,tundra4))))
    `(helm-ff-directory ((,class (:foreground ,tundra9 :weight bold))))
    `(helm-ff-dirs ((,class (:foreground ,tundra9))))
    `(helm-ff-dotted-director ((,class (:foreground ,tundra9 :underline t))))
    `(helm-ff-dotted-symlink-director ((,class (:foreground ,tundra7 :weight bold))))
    `(helm-ff-executable ((,class (:foreground ,tundra8))))
    `(helm-ff-file ((,class (:foreground ,tundra4))))
    `(helm-ff-invalid-symlink ((,class (:foreground ,tundra11 :weight bold))))
    `(helm-ff-prefix ((,class (:foreground ,tundra0 :background ,tundra9))))
    `(helm-ff-symlink ((,class (:foreground ,tundra7))))
    `(helm-grep-cmd-line ((,class (:foreground ,tundra4 :background ,tundra0))))
    `(helm-grep-file ((,class (:foreground ,tundra8))))
    `(helm-grep-finish ((,class (:foreground ,tundra5))))
    `(helm-grep-lineno ((,class (:foreground ,tundra4))))
    `(helm-grep-match ((,class (:inherit isearch))))
    `(helm-grep-running ((,class (:foreground ,tundra8))))
    `(helm-header ((,class (:foreground ,tundra9 :background ,tundra2))))
    `(helm-header-line-left-margin ((,class (:foreground ,tundra9 :background ,tundra2))))
    `(helm-history-deleted ((,class (:foreground ,tundra11))))
    `(helm-history-remote ((,class (:foreground ,tundra4))))
    `(helm-lisp-completion-info ((,class (:foreground ,tundra4 :weight bold))))
    `(helm-lisp-show-completion ((,class (:inherit isearch))))
    `(helm-locate-finish ((,class (:foreground ,tundra14))))
    `(helm-match ((,class (:foreground ,tundra8))))
    `(helm-match-item ((,class (:inherit isearch))))
    `(helm-moccur-buffer ((,class (:foreground ,tundra8))))
    `(helm-resume-need-update ((,class (:foreground ,tundra0 :background ,tundra13))))
    `(helm-selection ((,class (:inherit highlight))))
    `(helm-selection-line ((,class (:background ,tundra2))))
    `(helm-source-header ((,class (:height 1.44 :foreground ,tundra8 :background ,tundra2))))
    `(helm-swoop-line-number-face ((,class (:foreground ,tundra4 :background ,tundra0))))
    `(helm-swoop-target-word-face ((,class (:foreground ,tundra0 :background ,tundra7))))
    `(helm-swoop-target-line-face ((,class (:background ,tundra13 :foreground ,tundra3))))
    `(helm-swoop-target-line-block-face ((,class (:background ,tundra13 :foreground ,tundra3))))
    `(helm-separator ((,class (:background ,tundra2))))
    `(helm-visible-mark ((,class (:background ,tundra2))))

    ;; > Magit
    `(magit-branch ((,class (:foreground ,tundra7 :weight bold))))
    `(magit-diff-context-highlight ((,class (:background ,tundra2))))
    `(magit-diff-file-header ((,class (:foreground ,tundra8 :box (:color ,tundra8)))))
    `(magit-diffstat-added ((,class (:foreground ,tundra14))))
    `(magit-diffstat-removed ((,class (:foreground ,tundra11))))
    `(magit-hash ((,class (:foreground ,tundra8))))
    `(magit-hunk-heading ((,class (:foreground ,tundra9))))
    `(magit-hunk-heading-highlight ((,class (:foreground ,tundra9 :background ,tundra2))))
    `(magit-item-highlight ((,class (:foreground ,tundra8 :background ,tundra2))))
    `(magit-log-author ((,class (:foreground ,tundra7))))
    `(magit-process-ng ((,class (:foreground ,tundra13 :weight bold))))
    `(magit-process-ok ((,class (:foreground ,tundra14 :weight bold))))
    `(magit-section-heading ((,class (:foreground ,tundra7 :weight bold))))
    `(magit-section-highlight ((,class (:background ,tundra2))))

    ;; > MU4E
    `(mu4e-header-marks-face ((,class (:foreground ,tundra9))))
    `(mu4e-title-face ((,class (:foreground ,tundra8))))
    `(mu4e-header-key-face ((,class (:foreground ,tundra8))))
    `(mu4e-highlight-face ((,class (:highlight))))
    `(mu4e-flagged-face ((,class (:foreground ,tundra13))))
    `(mu4e-unread-face ((,class (:foreground ,tundra13 :weight bold))))
    `(mu4e-link-face ((,class (:underline t))))

    ;; > Powerline
    `(powerline-active1 ((,class (:foreground ,tundra4 :background ,tundra1))))
    `(powerline-active2 ((,class (:foreground ,tundra4 :background ,tundra3))))
    `(powerline-inactive1 ((,class (:background ,tundra2))))
    `(powerline-inactive2 ((,class (:background ,tundra2))))

    ;; > Powerline Evil
    `(powerline-evil-base-face ((,class (:foreground ,tundra4))))
    `(powerline-evil-normal-face ((,class (:background ,tundra8))))
    `(powerline-evil-insert-face ((,class (:foreground ,tundra0 :background ,tundra4))))
    `(powerline-evil-visual-face ((,class (:foreground ,tundra0 :background ,tundra7))))
    `(powerline-evil-replace-face ((,class (:foreground ,tundra0 :background ,tundra9))))

    ;; > NeoTree
    `(neo-banner-face ((,class (:foreground ,tundra10))))
    `(neo-dir-link-face ((,class (:foreground ,tundra9))))
    `(neo-expand-btn-face ((,class (:foreground ,tundra6 :bold t))))
    `(neo-file-link-face ((,class (:foreground ,tundra4))))
    `(neo-root-dir-face ((,class (:foreground ,tundra7 :weight bold))))
    `(neo-vc-added-face ((,class (:foreground ,tundra14))))
    `(neo-vc-conflict-face ((,class (:foreground ,tundra11))))
    `(neo-vc-default-face ((,class (:foreground ,tundra4))))
    `(neo-vc-edited-face ((,class (:foreground ,tundra13))))
    `(neo-vc-ignored-face ((,class (:foreground ,tundra3))))
    `(neo-vc-missing-face ((,class (:foreground ,tundra12))))
    `(neo-vc-needs-merge-face ((,class (:background ,tundra12 :foreground ,tundra4))))
    `(neo-vc-needs-update-face ((,class (:background ,tundra10 :foreground ,tundra4))))
    `(neo-vc-removed-face ((,class (:foreground ,tundra11 :strike-through nil))))
    `(neo-vc-up-to-date-face ((,class (:foreground ,tundra4))))
    `(neo-vc-user-face ((,class (:foreground ,tundra4))))

    ;; > Cider
    `(cider-result-overlay-face ((t (:background unspecified))))

    ;; > Org
    `(org-level-1 ((,class (:foreground ,tundra7 :weight extra-bold))))
    `(org-level-2 ((,class (:foreground ,tundra8 :weight bold))))
    `(org-level-3 ((,class (:foreground ,tundra9 :weight semi-bold))))
    `(org-level-4 ((,class (:foreground ,tundra10 :weight normal))))
    `(org-level-5 ((,class (:inherit org-level-4))))
    `(org-level-6 ((,class (:inherit org-level-4))))
    `(org-level-7 ((,class (:inherit org-level-4))))
    `(org-level-8 ((,class (:inherit org-level-4))))
    `(org-agenda-structure ((,class (:foreground ,tundra9))))
    `(org-agenda-date ((,class (:foreground ,tundra8 :underline nil))))
    `(org-agenda-done ((,class (:foreground ,tundra14))))
    `(org-agenda-dimmed-todo-face ((,class (:background ,tundra13))))
    `(org-block ((,class (:foreground ,tundra4))))
    `(org-block-background ((,class (:background ,tundra0))))
    `(org-block-begin-line ((,class (:foreground ,tundra7))))
    `(org-block-end-line ((,class (:foreground ,tundra7))))
    `(org-checkbox ((,class (:foreground ,tundra9))))
    `(org-checkbox-statistics-done ((,class (:foreground ,tundra14))))
    `(org-checkbox-statistics-todo ((,class (:foreground ,tundra13))))
    `(org-code ((,class (:foreground ,tundra7))))
    `(org-column ((,class (:background ,tundra2))))
    `(org-column-title ((,class (:inherit org-column :weight bold :underline t))))
    `(org-date ((,class (:foreground ,tundra8))))
    `(org-document-info ((,class (:foreground ,tundra4))))
    `(org-document-info-keyword ((,class (:foreground ,tundra3 :weight bold))))
    `(org-document-title ((,class (:foreground ,tundra8 :weight bold))))
    `(org-done ((,class (:foreground ,tundra14 :weight bold))))
    `(org-ellipsis ((,class (:foreground ,tundra3))))
    `(org-footnote ((,class (:foreground ,tundra8))))
    `(org-formula ((,class (:foreground ,tundra9))))
    `(org-hide ((,class (:foreground ,tundra0 :background ,tundra0))))
    `(org-link ((,class (:underline t))))
    `(org-scheduled ((,class (:foreground ,tundra14))))
    `(org-scheduled-previously ((,class (:foreground ,tundra13))))
    `(org-scheduled-today ((,class (:foreground ,tundra8))))
    `(org-special-keyword ((,class (:foreground ,tundra9))))
    `(org-table ((,class (:foreground ,tundra9))))
    `(org-todo ((,class (:foreground ,tundra13 :weight bold))))
    `(org-upcoming-deadline ((,class (:foreground ,tundra12))))
    `(org-warning ((,class (:foreground ,tundra13 :weight bold))))
    `(font-latex-bold-face ((,class (:inherit bold))))
    `(font-latex-italic-face ((,class (:slant italic))))
    `(font-latex-string-face ((,class (:foreground ,tundra14))))
    `(font-latex-match-reference-keywords ((,class (:foreground ,tundra9))))
    `(font-latex-match-variable-keywords ((,class (:foreground ,tundra4))))
    `(ido-only-match ((,class (:foreground ,tundra8))))
    `(org-sexp-date ((,class (:foreground ,tundra7))))
    `(ido-first-match ((,class (:foreground ,tundra8 :weight bold))))
    `(ido-subdir ((,class (:foreground ,tundra9))))
    `(org-quote ((,class (:inherit org-block :slant italic))))
    `(org-verse ((,class (:inherit org-block :slant italic))))
    `(org-agenda-date-weekend ((,class (:foreground ,tundra9))))
    `(org-agenda-date-today ((,class (:foreground ,tundra8 :weight bold))))
    `(org-agenda-done ((,class (:foreground ,tundra14))))
    `(org-verbatim ((,class (:foreground ,tundra7))))

    ;; > ivy-mode
    `(ivy-current-match ((,class (:inherit region))))
    `(ivy-minibuffer-match-face-1 ((,class (:inherit default))))
    `(ivy-minibuffer-match-face-2 ((,class (:background ,tundra7 :foreground ,tundra0))))
    `(ivy-minibuffer-match-face-3 ((,class (:background ,tundra8 :foreground ,tundra0))))
    `(ivy-minibuffer-match-face-4 ((,class (:background ,tundra9 :foreground ,tundra0))))
    `(ivy-remote ((,class (:foreground ,tundra14))))
    `(ivy-posframe ((,class (:background ,tundra1))))
    `(ivy-posframe-border ((,class (:background ,tundra1))))
    `(ivy-remote ((,class (:foreground ,tundra14))))

    ;; > perspective
    `(persp-selected-face ((,class (:foreground ,tundra8 :weight bold))))))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
    (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'tundra)

;; Local Variables:
;; no-byte-compile: t
;; indent-tabs-mode: nil
;; End:

;;; tundra-theme.el ends here
